#!/bin/sh
#
# This file is a bit tcos adapted. See .orig file for original version.
#
# This version here will be triggered by /etc/xdg/autostart/read_elotouch_calib.desktop,
# once the usersession is up and running.

echo "xEloInit.sh was runing on $(date)" > $HOME/xEloInit.status

elousbd_PID=`ps -C elousbd -o pid=`
eloautocalib_PID=`ps -C eloautocalib -o pid=`
 
if [ -n "$eloautocalib_PID" ]; then
	echo 'Killing existing eloautocalib processes in memory'
	killall eloautocalib
fi

# Uncomment the '/etc/opt/elo-usb/eloautocalib' line entry below to load the calibration values from 
# the monitor NVRAM on system startup. Type "# /etc/opt/elo-usb/eloautocalib --help" at command prompt for 
# help and options on eloautocalib utility.

# eloautocalib - not active [Default] - Does not read and apply calibration values from NVRAM.   
# eloautocalib - active               - Read calibration values from NVRAM and apply automatically. 

# Only start'eloautocalib' if 'elousbd' is running.
if [ -n "$elousbd_PID" ]; then
 # echo 'Loading Elo touch screen calibration data'
    /etc/opt/elo-usb/eloautocalib --renew
    echo 'xEloInit.sh found a running elousbd. Command "eloautocalib --renew" has been triggered.' >> $HOME/xEloInit.status
else
    echo 'xEloInit.sh found no running elousbd.' >> $HOME/xEloInit.status
fi
